To Start the Project 

* Run all commands in folder project.

1. Building the project 
 -> mvn clean install package -D maven.test.skip=true

2. Building the docker app image
 -> docker-compose build cliente

3. Run The Application
 -> docker-compose up


 Other instructions 

 1. To Clean e Reset the project 

 Remove all content to the folder 'docker/volume_mysql', try no commit the content.
 Remove all content to target folder

 -> With container stopped, execute 'docker container prune', 'docker image rm cliente/api'

 2. To Run in development mode

 To run in development mode in eclipse IDE, you need comment the 'cliente' service in the docker-compose file and change the database url in application.properties to 'localhost' 