CREATE TABLE CLIENTE (
  cd_cliente INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(45) NULL,
  cpf VARCHAR(45) NULL,
  data_nascimento VARCHAR(45) NULL,
  PRIMARY KEY (cd_cliente));
  
INSERT INTO CLIENTE(nome, cpf, data_nascimento) VALUES('Fabio Leal', '84022914150','27/11/1980');
INSERT INTO CLIENTE(nome, cpf, data_nascimento) VALUES('Sylvana Maria', '74022914140','16/02/1980');
INSERT INTO CLIENTE(nome, cpf, data_nascimento) VALUES('Mariah Aliceral', '64022914130','22/04/2014');
INSERT INTO CLIENTE(nome, cpf, data_nascimento) VALUES('Maria Aliceral', '54022914120','20/05/2016');

