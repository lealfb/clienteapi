package io.builders.cliente.controller.form;

import io.builders.cliente.modelo.Cliente;
import io.builders.cliente.repository.ClienteRepository;

/** Classe utilizada para proporcionar certa flexibilidade.
 * Aqui inferimos que o cpf uma vez cadastrado não pode ser mais alterado.*/
public class AtualizacaoClienteForm {
	
	private String nome;
	private String dataNascimento;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public Cliente atualizar(Long id, ClienteRepository clienteRepository) {
		Cliente cliente = clienteRepository.getOne(id);
		
		cliente.setNome(this.nome);
		cliente.setDataNascimento(this.dataNascimento);
		return cliente;
	}

}
