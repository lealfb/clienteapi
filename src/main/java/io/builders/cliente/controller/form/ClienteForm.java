package io.builders.cliente.controller.form;


import io.builders.cliente.modelo.Cliente;
import io.builders.cliente.repository.ClienteRepository;

public class ClienteForm {

	private String nome;
	private String cpf;
	private String dataNascimento;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Cliente converter() {
		return new Cliente(nome, cpf, dataNascimento);
	}
	
	public Cliente atualizar(Long id, ClienteRepository clienteRepository) {
		Cliente cliente = clienteRepository.getOne(id);
		cliente.setNome(this.nome);
		cliente.setCpf(this.cpf);
		cliente.setDataNascimento(this.dataNascimento);
		return cliente;
	}

}
