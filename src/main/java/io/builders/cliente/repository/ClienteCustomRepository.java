package io.builders.cliente.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import io.builders.cliente.modelo.Cliente;

@Repository
public class ClienteCustomRepository {

	private final EntityManager em;
	
    public ClienteCustomRepository(EntityManager em) {
        this.em = em;
    }

    public List<Cliente> find(Long id, String nome, String cpf, String dataNascimento) {

        String query = "select C from Cliente as C ";
        String condicao = "where";

        if(id != null) {
            query += condicao + " P.id = :id";
            condicao = " and ";
        }
        
        if(nome != null) {
            query += condicao + " C.nome = :nome";
            condicao = " and ";
        }

        if(cpf != null) {
            query += condicao + " C.cpf = :cpf";
            condicao = " and ";
        }
        
        if(dataNascimento != null) {
            query += condicao + " C.dataNascimento = :dataNascimento";
        }

        TypedQuery<Cliente> q = em.createQuery(query, Cliente.class);

        if(id != null) {
            q.setParameter("id", id);
        }

        if(nome != null) {
            q.setParameter("nome", nome);
        }

        if(cpf != null) {
            q.setParameter("cpf", cpf);
        }
        
        if(dataNascimento != null) {
            q.setParameter("dataNascimento", dataNascimento);
        }

        return q.getResultList();
    }
	
}
