package io.builders.cliente.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.builders.cliente.modelo.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
	Page<Cliente> findByNome(String nome, Pageable paginacao);
	Page<Cliente> findByCpf(String cpf, Pageable paginacao);
		
}
